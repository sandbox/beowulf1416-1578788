/**
 * extjs charts integration with node js
 * refreshes the charts with updated data
 */
(function($){
	Drupal.extjs.callbacks.chart_refresh = {
		callback: function(message){
			Ext.each(Ext.ComponentQuery.query("chart"), function(chart){
				chart.getStore().load();
			});
		}	
	};
})(jQuery);