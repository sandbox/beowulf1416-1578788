<?php
class ExtJsChartTest extends DrupalWebTestCase {
	public static function getInfo(){
		return array(
			"name"=>"ExtJs Chart Unit Tests",
			"description"=>"ExtJs Chart unit tests",
			"group"=>"extjs"
		);
	}
	
	function setUp(){
		parent::setUp("extjs");
	}
	
	function test_crud_charts(){
		$o = new stdClass();
		$o->name = "Chart test";
		$o->view_name = "test";
		$o->service_endpoint = "test";
		$o->data_format = "json";
		$o->height = 300;
		$o->widtdh = 300;
		$o->legend_position = CHART_LEGEND_POSITION_LEFT;
		
		// add chart
		$chart_id = null;
		try {
			$chart_id = extjs_chart_add($o);
			$this->assertNotNull($chart_id);
		} catch(Exception $e){
			$this->fail($e->getMessage());
		}
		
		// retrieve chart
		try {
			$result = extjs_chart_load($chart_id);
			$this->assertNotNull($result);
		} catch(Exception $e){
			$this->fail($e->getMessage());
		}
		
		
	}
}