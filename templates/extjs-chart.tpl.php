<?php
/**
 * extjs chart template
 * variables:
 * 	$chart
 */
$chart_id = $chart->id;
?>
<div id="chart-<?php print($chart_id); ?>">
<?php extjs_chart_embed("chart-{$chart_id}", $chart); ?>
</div><!-- #chart-<?php print($chart_id); ?> -->