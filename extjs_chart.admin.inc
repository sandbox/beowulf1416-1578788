<?php
function extjs_chart_form($form, $form_state, $chart = null){
	$form["chart"] = array(
		"#type"=>"value",
		"#value"=>$chart
	);
	
	$form["name"] = array(
		"#type"=>"machine_name",
		"#title"=>t("Name"),
		"#required"=>true,
		"#machine_name"=>array(
			"exists"=>"_extjs_chart_name_exists"
		),
		"#default_value"=>isset($form_state["values"]["name"]) ? $form_state["values"]["name"] : (isset($chart->name) ? $chart->name : "")
	);
	
	$form["description"] = array(
		"#type"=>"textfield",
		"#title"=>t("Description"),
		"#required"=>false,
		"#default_value"=>isset($form_state["values"]["description"]) ? $form_state["values"]["description"] : (isset($chart->description) ? $chart->description : "")
	);
	
	$form["data-source"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Data Source")
	);
	
	$options = drupal_map_assoc(array_keys(services_endpoint_load_all()));
	$form["data-source"]["service-endpoint"] = array(
		"#type"=>"select",
		"#title"=>t("Service Endpoint"),
		"#required"=>true,
		"#options"=>$options,
		"#default_value"=>isset($form_state["values"]["service-endpoint"]) ? $form_state["values"]["service-endpoint"] : (isset($chart->service_endpoint) ? $chart->service_endpoint : 0)
	);
	
	$form["data-source"]["format"] = array(
		"#type"=>"select",
		"#title"=>t("Format"),
		"#required"=>true,
		"#options"=>array(
			"json"=>t("json"),
			"xml"=>t("XML")
		),
		"#default_value"=>isset($form_state["values"]["format"]) ? $form_state["values"]["format"] : (isset($chart->data_format) ? $chart->data_format : "")
	);
	
	$form["data-source"]["root"] = array(
		"#type"=>"textfield",
		"#title"=>t("Root"),
		"#required"=>false,
		"#default_value"=>""
	);
	
	$views = views_get_enabled_views();
	$options = array();
	foreach($views as $v){
		$options[$v->name] = $v->name;
	}
	$default_view = isset($form_state["values"]["view"]) ? $form_state["values"]["view"] : (isset($chart->view_name) ? $chart->view_name : null);
	$form["data-source"]["view"] = array(
		"#type"=>"select",
		"#title"=>t("View"),
		"#required"=>true,
		"#options"=>$options,
		"#default_value"=>$default_view,
		"#ajax"=>array(
			"callback"=>"extjs_chart_view_callback",
			"wrapper"=>"view-wrapper",
			"method"=>"replace"
		)
	);
	
	$form["data-source"]["fields"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Fields"),
		"#prefix"=>"<div id=\"view-wrapper\">\n",
		"#suffix"=>"</div><!-- #view-wrapper -->",
		"#tree"=>true
	);
	
	if($default_view != ""){
		$view = views_get_view($default_view);
		$views_data = views_fetch_data($view->base_table);
		$base_table = $view->base_table;
		$base_field = $views_data["table"]["base"]["field"];
		$views_fields = $view->display["default"]->display_options["fields"];
		
		foreach($views_fields as $field_id=>$field){
			$field_key = $base_field == $field_id ? $field_id : "{$base_table}_{$field_id}";
				
			$types = array(
				"auto"=>t("Auto"),
				"string"=>t("String"),
				"int"=>t("Integer"),
				"float"=>t("Float"),
				"boolean"=>t("Boolean"),
				"date"=>t("Date")
			);
				
			$default_type = isset($chart->fields) && isset($chart->fields[$field_key]) ? $chart->fields[$field_key] : "auto";
			$form["data-source"]["fields"][$field_key] = array(
				"#type"=>"select",
				"#title"=>t("%field Type", array("%field"=>$field_key)),
				"#options"=>$types,
				"#default_value"=>$default_type
			);
		}
	}
	
	$form["legend-group"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Legend"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$form["legend-group"]["legend-position"] = array(
		"#type"=>"select",
		"#title"=>t("Position"),
		"#required"=>true,
		"#options"=>array(
			CHART_LEGEND_POSITION_LEFT=>t("Left"),
			CHART_LEGEND_POSITION_RIGHT=>t("Right"),
			CHART_LEGEND_POSITION_TOP=>t("Top"),
			CHART_LEGEND_POSITION_BOTTOM=>t("Bottom")
		),
		"#default_value"=>isset($form_state["values"]["legend-position"]) ? $form_state["values"]["legend-position"] : (isset($chart->legend_position) ? $chart->legend_position : CHART_LEGEND_POSITION_LEFT)
	);
	
	$form["series-group"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Series")
	);
	
	$options = array();
	$series = isset($chart->series) ? $chart->series : array();
	foreach($series as $s){
		$options[$s->id] = array(
			"type"=>$s->type,
			"title"=>l($s->title, "chart/{$chart->id}/series/{$s->id}/edit")
		);
	
	}
	$form["series-group"]["series"] = array(
		"#type"=>"tableselect",
		"#header"=>array(
			"title"=>array(
				"data"=>t("Title"),
				"field"=>"title"
			),
			"type"=>array(
				"data"=>t("Type"),
				"field"=>"type"
			)
		),
		"#multiple"=>true,
		"#options"=>$options,
		"#default_value"=>array(),
		"#empty"=>isset($chart->id) ? l(t("No series defined. Add series."), "chart/{$chart->id}/series/add") : t("Save chart")
	);
	
	if(isset($chart->id)){
		$form["series-group"]["actions"] = array();
		
		$form["series-group"]["actions"]["add"] = array(
			"#type"=>"submit",
			"#value"=>t("Add Series"),
			"#name"=>"btn-series-add"
		);
		
		$form["series-group"]["actions"]["remove"] = array(
			"#type"=>"submit",
			"#value"=>t("Remove Series"),
			"#name"=>"btn-series-remove"
		);
	}
	
	$form["axis-group"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Axes")
	);
	
	$options = array();
	if(isset($chart->axes)){
		foreach($chart->axes as $a){
			$options[$a->id] = array(
				"title"=>l("{$a->title}","chart/{$chart->id}/axis/{$a->id}/edit"),
				"type"=>$a->type
			);
		}
	}
	$form["axis-group"]["axes"] = array(
		"#type"=>"tableselect",
		"#title"=>t("Axes"),
		"#header"=>array(
			"title"=>array(
				"data"=>t("Title"),
				"field"=>"title"
			),
			"type"=>array(
				"data"=>t("Type"),
				"field"=>"type"
			)
		),
		"#options"=>$options
	);
	
	if(isset($chart->id)){
		$form["axis-group"]["actions"] = array();
		
		$form["axis-group"]["actions"]["add"] = array(
			"#type"=>"submit",
			"#value"=>t("Add Axis"),
			"#name"=>"btn-axis-add"
		);
		
		$form["axis-group"]["actions"]["remove"] = array(
			"#type"=>"submit",
			"#value"=>t("Remove Axis"),
			"#name"=>"btn-axis-remove"
		);
	}
	
	$form["style"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Style"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$form["style"]["style-height"] = array(
		"#type"=>"textfield",
		"#title"=>t("Height"),
		"#required"=>true,
		"#default_value"=>isset($form_state["values"]["style-height"]) ? $form_state["values"]["style-height"] : (isset($chart->height) ? $chart->height : 300)
	);
	
	$form["style"]["style-width"] = array(
		"#type"=>"textfield",
		"#title"=>t("Width"),
		"#required"=>true,
		"#default_value"=>isset($form_state["values"]["style-width"]) ? $form_state["values"]["style-width"] : (isset($chart->width) ? $chart->width : 300)
	);
	
	$form["actions"]["save"] = array(
		"#type"=>"submit",
		"#value"=>t("Save"),
		"#name"=>"btn-save"
	);

	return $form;
}

function extjs_chart_view_callback(&$form, &$form_state){
	return $form["data-source"]["fields"];
}

function extjs_chart_form_validate(&$form, &$form_state){
	
}

function extjs_chart_form_submit(&$form, &$form_state){
	$chart = $form_state["values"]["chart"];
	
	switch($form_state["triggering_element"]["#name"]){
		case "btn-series-add":{
			_extjs_chart_form_add_series($form, $form_state, $chart);
			break;
		}
		case "btn-series-remove":{
			_extjs_chart_form_remove_series($form, $form_state, $chart);
			break;
		}
		case "btn-axis-add":{
			_extjs_chart_form_add_axis($form, $form_state, $chart);
			break;
		}
		case "btn-save":{
			_extjs_chart_form_save($form, $form_state, $chart);
			break;
		}
	}
	
}

function _extjs_chart_form_save(&$form, &$form_state, $chart){
	$o = new stdClass();
	$o->name = $form_state["values"]["name"];
	$o->description = $form_state["values"]["description"];
	$o->view_name = $form_state["values"]["view"];
	$o->service_endpoint = $form_state["values"]["service-endpoint"];
	$o->data_format = $form_state["values"]["format"];
	$o->height = $form_state["values"]["style-height"];
	$o->width = $form_state["values"]["style-width"];
	$o->legend_position = $form_state["values"]["legend-position"];
	$o->fields = $form_state["values"]["fields"];
	
	if(isset($chart->id)){
		try {
			$o->id = $chart->id;
			extjs_chart_update($o);
			drupal_set_message(t("Updated chart"));
				
			$form_state["redirect"] = "chart/{$o->id}";
		} catch(Exception $e){
			watchdog_exception("extjs_chart", $e);
			drupal_set_message(t("Unable to update chart"), "error");
		}
	} else {
		try {
			$o->id = extjs_chart_add($o);
			drupal_set_message(t("Added chart"));
				
			$form_state["redirect"] = "chart/{$o->id}";
		} catch(Exception $e){
			watchdog_exception("extjs_chart", $e);
			drupal_set_message(t("Unable to add chart"), "error");
		}
	}
}

function _extjs_chart_form_add_series(&$form, &$form_state, $chart){
	$form_state["redirect"] = "chart/{$chart->id}/series/add";
}

function _extjs_chart_form_remove_series(&$form, &$form_state, $chart){
	foreach($form_state["values"]["series"] as $series_id=>$selected){
		if($selected){
			$o = new stdClass();
			$o->id = $series_id;
			
			extjs_chart_series_remove($chart, $o);
		}
	}
}

function _extjs_chart_form_add_axis(&$form, &$form_state, $chart){
	$form_state["redirect"] = "chart/{$chart->id}/axis/add";
}

function extjs_chart_series_form($form, $form_state, $chart = null, $series = null){
	$view = views_get_view($chart->view_name);
	
	$form["chart"] = array(
		"#type"=>"value",
		"#value"=>$chart
	);
	
	$form["series"] = array(
		"#type"=>"value",
		"#value"=>$series
	);
	
	$form["title"] = array(
		"#type"=>"textfield",
		"#title"=>t("Title"),
		"#required"=>true,
		"#default_value"=>isset($form_state["values"]["title"]) ? $form_state["values"]["title"] : (isset($series->title) ? $series->title : "")
	);
	
	$form["series-type"] = array(
		"#type"=>"select",
		"#title"=>t("Series Type"),
		"#required"=>true,
		"#options"=>array(
			CHART_SERIES_TYPE_AREA=>t("Area"),
			CHART_SERIES_TYPE_LINE=>t("Line"),
			CHART_SERIES_TYPE_BAR=>t("Bar")
		),
		"#default_value"=>isset($form_state["values"]["series-type"]) ? $form_state["values"]["series-type"] : (isset($series->type) ? $series->type : null),
		"#ajax"=>array(
			"callback"=>"extjs_chart_series_callback",
			"wrapper"=>"series-wrapper"
		)
	);
	
	$form["configs"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Configuration"),
		"#prefix"=>"<div id=\"series-wrapper\">\n",
		"#suffix"=>"</div><!-- #series-wrapper -->",
		"#tree"=>true
	);
	
	switch($form["series-type"]["#default_value"]){
		case CHART_SERIES_TYPE_LINE:{
			_extjs_chart_series_line($form, $form_state, $chart, $series);
			break;
		}
		case CHART_SERIES_TYPE_BAR:{
			_extjs_chart_series_bar($form, $form_state, $chart, $series);
			break;
		}
		case CHART_SERIES_TYPE_AREA:{
			_extjs_chart_series_area($form, $form_state, $chart, $series);
			break;
		}
	}
	
	$form["actions"]["save"] = array(
		"#type"=>"submit",
		"#value"=>t("Save"),
		"#name"=>"btn-save"
	);
	
	return $form;
}

function extjs_chart_series_callback(&$form, &$form_state){
	return $form["configs"];
}

function _extjs_chart_series_area(&$form, &$form_state, $chart = null, $series = null){
	$form["configs"]["highlight"] = array(
		"#type"=>"checkbox",
		"#title"=>t("Highlight"),
		"#default_value"=>isset($form_state["values"]["highlight"]) ? $form_state["values"]["highlight"] : (isset($series->highlight) ? $series->highlight : false)
	);
	
	$form["configs"]["show-in-legend"] = array(
		"#type"=>"checkbox",
		"#title"=>t("Show in legend"),
		"#default_value"=>isset($form_state["values"]["show-in-legend"]) ? $form_state["values"]["show-in-legend"] : (isset($series->show_in_legend) ? $series->show_in_legend : false)
	);
	
	$form["configs"]["axis"] = array(
		"#type"=>"select",
		"#title"=>t("Axis"),
		"#required"=>true,
		"#options"=>array(
			CHART_AXIS_POSITION_LEFT=>t("Left"),
			CHART_AXIS_POSITION_RIGHT=>t("Right"),
			CHART_AXIS_POSITION_TOP=>t("Top"),
			CHART_AXIS_POSITION_BOTTOM=>t("Bottom")
		),
		"#default_value"=>isset($form_state["values"]["configs"]["axis"]) ? $form_state["values"]["configs"]["axis"] : (isset($series->configuration["axis"]) ? $series->configuration["axis"] : null)
	);
	
	$fields = array();
	foreach($chart->fields as $field_id=>$field_type){
		$fields[$field_id] = check_plain($field_id);
	}
	$form["configs"]["field-x"] = array(
		"#type"=>"select",
		"#title"=>t("X Field"),
		"#required"=>true,
		"#options"=>$fields,
		"#default_value"=>isset($form_state["values"]["configs"]["field-x"]) ? $form_state["values"]["configs"]["field-x"] : (isset($series->configuration["field_x"]) ? $series->configuration["field_x"] : null)
	);
	
	$options = array();
	foreach($fields as $field){
		$options[$field] = array(
			"field_name"=>check_plain($field)
		);
	}
	$defaults = isset($form_state["values"]["configs"]["field-y"]) ? $form_state["values"]["configs"]["field-y"] : (isset($series->configuration["field_y"]) ? $series->configuration["field_y"] : array());
	foreach($defaults as $key=>$value){
		$defaults[$key] = $key == $value;
	}
	$form["configs"]["field-y"] = array(
		"#type"=>"tableselect",
		"#title"=>t("Y Field"),
		"#required"=>true,
		"#header"=>array(
			"field_name"=>array(
				"data"=>t("Field Name"),
				"field"=>"field_name"
			)
		),
		"#options"=>$options,
		"#default_value"=>$defaults
	);
	
	// label
	$form["configs"]["label"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Label"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$form["configs"]["label"]["display"] = array(
		"#type"=>"select",
		"#title"=>t("Display"),
		"#options"=>array(
			"rotate"=>t("Rotate"),
			"middle"=>t("Middle"),
			"insideStart"=>t("Inside Start"),
			"insideEnd"=>t("Inside End"),
			"outside"=>t("Outside"),
			"over"=>t("Over"),
			"under"=>t("Under"),
			"none"=>t("None")
		),
		"#default_value"=>"none"
	);
	
	$form["configs"]["label"]["color"] = array(
		"#type"=>"textfield",
		"#title"=>t("Color")
	);
	
	$form["configs"]["label"]["field"] = array(
		"#type"=>"select",
		"#title"=>t("Field"),
		"#options"=>$fields
	);
	
	$form["configs"]["label"]["orientation"] = array(
		"#type"=>"select",
		"#title"=>t("Orientation"),
		"#options"=>array(
			"horizontal"=>t("Horizontal"),
			"vertical"=>t("Vertical")
		)
	);
	
	$form["configs"]["label"]["font"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Font"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$form["configs"]["label"]["font"]["name"] = array(
		"#type"=>"textfield",
		"#title"=>t("Name")
	);
	
	$form["configs"]["label"]["font"]["size"] = array(
		"#type"=>"textfield",
		"#title"=>t("Size")
	);
	
}

function _extjs_chart_series_line(&$form, &$form_state, $chart = null, $series = null){
	$form["configs"]["highlight"] = array(
		"#type"=>"checkbox",
		"#title"=>t("Highlight"),
		"#default_value"=>isset($form_state["values"]["highlight"]) ? $form_state["values"]["highlight"] : (isset($series->highlight) ? $series->highlight : false)
	);
	
	$form["configs"]["show-in-legend"] = array(
		"#type"=>"checkbox",
		"#title"=>t("Show in legend"),
		"#default_value"=>isset($form_state["values"]["show-in-legend"]) ? $form_state["values"]["show-in-legend"] : (isset($series->show_in_legend) ? $series->show_in_legend : false)
	);
	
	$form["configs"]["axis"] = array(
		"#type"=>"select",
		"#title"=>t("Axis"),
		"#required"=>true,
		"#options"=>array(
			CHART_AXIS_POSITION_LEFT=>t("Left"),
			CHART_AXIS_POSITION_RIGHT=>t("Right"),
			CHART_AXIS_POSITION_TOP=>t("Top"),
			CHART_AXIS_POSITION_BOTTOM=>t("Bottom")
		),
		"#default_value"=>isset($form_state["values"]["configs"]["axis"]) ? $form_state["values"]["configs"]["axis"] : (isset($series->configuration["axis"]) ? $series->configuration["axis"] : null)
	);
	
	$fields = array();
	foreach($chart->fields as $field_id=>$field_type){
		$fields[$field_id] = check_plain($field_id);
	}
	$form["configs"]["field-x"] = array(
		"#type"=>"select",
		"#title"=>t("X Field"),
		"#required"=>true,
		"#options"=>$fields,
		"#default_value"=>isset($form_state["values"]["configs"]["field-x"]) ? $form_state["values"]["configs"]["field-x"] : (isset($series->configuration["field_x"]) ? $series->configuration["field_x"] : null)
	);
	
	$form["configs"]["field-y"] = array(
		"#type"=>"select",
		"#title"=>t("Y Field"),
		"#required"=>true,
		"#options"=>$fields,
		"#default_value"=>isset($form_state["values"]["configs"]["field-y"]) ? $form_state["values"]["configs"]["field-y"] : (isset($series->configuration["field_y"]) ? $series->configuration["field_y"] : null)
	);
	
	$form["configs"]["label"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Label"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$form["configs"]["label"]["display"] = array(
		"#type"=>"select",
		"#title"=>t("Display"),
		"#options"=>array(
			"rotate"=>t("Rotate"),
			"middle"=>t("Middle"),
			"insideStart"=>t("Inside Start"),
			"insideEnd"=>t("Inside End"),
			"outside"=>t("Outside"),
			"over"=>t("Over"),
			"under"=>t("Under"),
			"none"=>t("None")
		),
		"#default_value"=>"none"
	);
	
	$form["configs"]["label"]["color"] = array(
		"#type"=>"textfield",
		"#title"=>t("Color")
	);
	
	$form["configs"]["label"]["field"] = array(
		"#type"=>"select",
		"#title"=>t("Field"),
		"#options"=>$fields
	);
	
	$form["configs"]["label"]["orientation"] = array(
		"#type"=>"select",
		"#title"=>t("Orientation"),
		"#options"=>array(
			"horizontal"=>t("Horizontal"),
			"vertical"=>t("Vertical")
		)
	);
	
	$form["configs"]["label"]["font"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Font"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$form["configs"]["label"]["font"]["name"] = array(
		"#type"=>"textfield",
		"#title"=>t("Name")
	);
	
	$form["configs"]["label"]["font"]["size"] = array(
		"#type"=>"textfield",
		"#title"=>t("Size")
	);
	
	$form["configs"]["highlight"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Highlight"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$form["configs"]["highlight"]["type"] = array(
		"#type"=>"select",
		"#title"=>t("Type"),
		"#options"=>array(
			"circle"=>t("Circle"),
//			"path"=>t("Path"),
			"rect"=>t("Rectangle"),
			"text"=>t("Text"),
			"square"=>t("Square"),
			"image"=>t("Image")
		)
	);
	
	$form["configs"]["highlight"]["fill"] = array(
		"#type"=>"textfield",
		"#title"=>t("Fill Color")
	);
	
	$form["configs"]["highlight"]["height"] = array(
		"#type"=>"textfield",
		"#title"=>t("Height"),
		"#description"=>t("Used to set the height of the rectangle")
	);
	
	$form["configs"]["highlight"]["width"] = array(
		"#type"=>"textfield",
		"#title"=>t("width"),
		"#description"=>t("Used to set the width of the rectangle")
	);
	
	$form["configs"]["highlight"]["radius"] = array(
		"#type"=>"textfield",
		"#title"=>t("Radius"),
		"#description"=>t("Used to set the radius of circles")
	);
	
	$form["configs"]["marker"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Marker"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$form["configs"]["marker"]["show"] = array(
		"#type"=>"checkbox",
		"#title"=>t("Show markers")
	);
	
	$form["configs"]["marker"]["type"] = array(
		"#type"=>"select",
		"#title"=>t("Type"),
		"#options"=>array(
			"circle"=>t("Circle"),
//			"path"=>t("Path"),
			"rect"=>t("Rectangle"),
			"text"=>t("Text"),
			"square"=>t("Square"),
			"image"=>t("Image")
		)
	);
	
	$form["configs"]["marker"]["fill"] = array(
		"#type"=>"textfield",
		"#title"=>t("Fill Color")
	);
	
	$form["configs"]["marker"]["height"] = array(
		"#type"=>"textfield",
		"#title"=>t("Height"),
		"#description"=>t("Used to set the height of the rectangle")
	);
	
	$form["configs"]["marker"]["width"] = array(
		"#type"=>"textfield",
		"#title"=>t("width"),
		"#description"=>t("Used to set the width of the rectangle")
	);
	
	$form["configs"]["marker"]["radius"] = array(
		"#type"=>"textfield",
		"#title"=>t("Radius"),
		"#description"=>t("Used to set the radius of circles")
	);
	
	$form["configs"]["smooth"] = array(
		"#type"=>"textfield",
		"#title"=>t("Smooth")
	);
	
	$form["configs"]["style"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Style"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$form["configs"]["style"]["stroke-color"] = array(
		"#type"=>"textfield",
		"#title"=>t("Stroke Color")
	);
	
	$form["configs"]["style"]["stroke-width"] = array(
		"#type"=>"textfield",
		"#title"=>t("Stroke Width")
	);
	
	$form["configs"]["style"]["fill"] = array(
		"#type"=>"textfield",
		"#title"=>t("Fill Color")
	);
	
	$form["configs"]["style"]["opacity"] = array(
		"#type"=>"textfield",
		"#title"=>t("Opacity"),
		"#description"=>t("0 to 1.0")
	);
/*	
	$form["configs"]["tooltip"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Tooltip"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
*/	
}

function _extjs_chart_series_bar(&$form, &$form_state, $chart = null, $series = null){
	
}

function extjs_chart_series_form_validate(&$form, &$form_state){
	
}

function extjs_chart_series_form_submit(&$form, &$form_state){
	$chart = $form_state["values"]["chart"];
	$series = $form_state["values"]["series"];
	
	$o = new stdClass();
	$o->chart_id = $chart->id;
	$o->type = $form_state["values"]["series-type"];
	$o->title = $form_state["values"]["title"];
	
	switch($o->type){
		case CHART_SERIES_TYPE_LINE:{
			_extjs_chart_series_submit_line($form, $form_state, $o);
			break;
		}
		case CHART_SERIES_TYPE_BAR:{
			_extjs_chart_series_submit_bar($form, $form_state, $o);
			break;
		}
	}
	
	if(isset($series->id)){
		$o->id = $series->id;
		try {
			extjs_chart_series_update($chart, $o);
			$form_state["redirect"] = "chart/{$chart->id}";
			drupal_set_message(t("Updated chart series."));
		} catch(Exception $e) {
			watchdog_exception("extjs_chart", $e);
			drupal_set_message(t("An error occurred while updating chart"), "error");
		}
	} else {
		try {
			extjs_chart_series_add($chart, $o);
			$form_state["redirect"] = "chart/{$chart->id}";
			drupal_set_message(t("Added chart series"));
		} catch(Exception $e){
			watchdog_exception("extjs_chart", $e);
			drupal_set_message(t("An error occurred while trying to add a chart series"), "error");
		}
	}
	
}

function _extjs_chart_series_submit_line(&$form, &$form_state, &$series){
	$config = new stdClass();
	
	$config->highlight = $form_state["values"]["configs"]["highlight"];
	$config->show_in_legend = $form_state["values"]["configs"]["show-in-legend"];
	$config->axis = $form_state["values"]["configs"]["axis"];
	$config->field_x = $form_state["values"]["configs"]["field-x"];
	$config->field_y = $form_state["values"]["configs"]["field-y"];
	$config->label = $form_state["values"]["configs"]["label"];
	
	$series->configuration = $config;
}

function _extjs_chart_series_submit_bar(&$form, &$form_state, &$series){

}


function extjs_chart_axis_form($form, $form_state, $chart = null, $axis = null){
	$form["chart"] = array(
		"#type"=>"value",
		"#value"=>$chart
	);
	
	$form["axis"] = array(
		"#type"=>"value",
		"#value"=>$axis
	);
	
	$form["title"] = array(
		"#type"=>"textfield",
		"#title"=>t("Title"),
		"#required"=>true,
		"#default_value"=>isset($form_state["values"]["title"]) ? $form_state["values"]["title"] : (isset($axis->title) ? $axis->title : "")
	);
	
	$form["type"] = array(
		"#type"=>"select",
		"#title"=>t("Type"),
		"#required"=>true,
		"#options"=>array(
			CHART_AXIS_TYPE_ABSTRACT=>t("Abstract"),
			CHART_AXIS_TYPE_CATEGORY=>t("Category"),
			CHART_AXIS_TYPE_GAUGE=>t("Gauge"),
			CHART_AXIS_TYPE_NUMERIC=>t("Numeric"),
			CHART_AXIS_TYPE_RADIAL=>t("Radial"),
			CHART_AXIS_TYPE_TIME=>t("Time")
		),
		"#default_value"=>isset($form_state["values"]["type"]) ? $form_state["values"]["type"] : (isset($axis->type) ? $axis->type : null),
		"#ajax"=>array(
			"callback"=>"extjs_chart_axis_callback",
			"wrapper"=>"axis-wrapper"
		)
	);
	
	$form["config"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Configuration"),
		"#prefix"=>"<div id=\"axis-wrapper\">\n",
		"#suffix"=>"</div><!-- #axis-wrapper -->",
		"#tree"=>true
	);
	
	if(isset($form["type"]["#default_value"])){
		switch($form["type"]["#default_value"]){
			case CHART_AXIS_TYPE_NUMERIC:{
				_extjs_chart_axis_numeric($form, $form_state);
				break;
			}
		}
	}
	
	$form["actions"]["save"] = array(
		"#type"=>"submit",
		"#value"=>t("Save"),
		"#name"=>"btn-save"
	);
	
	return $form;
}

function extjs_chart_axis_callback(&$form, &$form_State){
	return $form["config"];
}

function _extjs_chart_axis_numeric(&$form, &$form_state){
	$chart = $form["chart"]["#value"];
	$axis = $form["axis"]["#value"];
	
	$options = array();
	foreach($chart->fields as $fid=>$field_type){
		$options[$fid] = array(
			"field"=>check_plain($fid)
		);
	}
	
	$default_value = array();
	if(isset($axis) && isset($axis->configuration["fields"])){
		foreach($axis->configuration["fields"] as $field_key=>$field_value){
			if($field_key == $field_value){
				$default_value[$field_key] = 1;
			}
		}
	}
	
	$form["config"]["fields"] = array(
		"#type"=>"tableselect",
		"#title"=>t("Field"),
		"#header"=>array(
			"field"=>array(
				"data"=>t("Field"),
				"field"=>"field"
			)
		),
		"#options"=>$options,
		"#default_value"=>$default_value
	);
	
	$default_value = array();
	if(isset($axis) && isset($axis->configuration["fields"])){
		foreach($axis->configuration["fields"] as $field_key=>$field_value){
			if($field_key == $field_value){
				$default_value[$field_key] = 1;
			}
		}
	}
	$form["config"]["position"] = array(
		"#type"=>"select",
		"#title"=>t("Position"),
		"#required"=>true,
		"#options"=>array(
			CHART_AXIS_POSITION_LEFT=>t("Left"),
			CHART_AXIS_POSITION_RIGHT=>t("Right"),
			CHART_AXIS_POSITION_TOP=>t("Top"),
			CHART_AXIS_POSITION_BOTTOM=>t("Bottom")
		),
		"#default_value"=>isset($form_state["values"]["config"]["position"]) ? $form_state["values"]["config"]["position"] : (isset($axis->configuration["position"]) ? $axis->configuration["position"] : null)
	);
	
	$form["config"]["grid"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Grid"),
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$form["config"]["grid"]["grid-enable"] = array(
		"#type"=>"checkbox",
		"#title"=>t("Grid")
	);
	
	$form["config"]["decimals"] = array(
		"#type"=>"textfield",
		"#title"=>t("Decimals"),
		"#default_value"=>isset($form_state["values"]["config"]["decimals"]) ? $form_state["values"]["config"]["decimals"] : (isset($axis->configuration["decimals"]) ? $axis->configuration["decimals"] : 2)
	);
	
	$form["config"]["minimum"] = array(
		"#type"=>"textfield",
		"#title"=>t("Minimum")
	);
	
	$form["config"]["maximum"] = array(
		"#type"=>"textfield",
		"#title"=>t("Maximum")
	);
	
	$form["config"]["major-ticks"] = array(
		"#type"=>"textfield",
		"#title"=>t("Major Tick Steps"),
	);
	
	$form["config"]["minor-ticks"] = array(
		"#type"=>"textfield",
		"#title"=>t("Minor Tick Steps")
	);
}

function extjs_chart_axis_form_validate(&$form, &$form_state){
	
}

function extjs_chart_axis_form_submit(&$form, &$form_state){
	$chart = $form_state["values"]["chart"];
	$axis = $form_state["values"]["axis"];
	
	$o = new stdClass();
	$o->title = $form_state["values"]["title"];
	$o->type = $form_state["values"]["type"];
	
	switch($o->type){
		case CHART_AXIS_TYPE_NUMERIC:{
			_extjs_chart_axis_submit_numeric($form, $form_state, $o);
			break;
		}
	}
	
	if(isset($axis->id)){
		try {
			$o->id = $axis->id;
			extjs_chart_axis_update($chart, $o);
			$form_state["redirect"] = "chart/{$chart->id}";
			drupal_set_message(t("Updated chart axis"));
		} catch(Exception $e){
			watchdog_exception("extjs_chart", $e);
			drupal_set_message(t("An error occurred while updating chart axis"), "error");
		}
	} else {
		try {
			extjs_chart_axis_add($chart, $o);
			$form_state["redirect"] = "chart/{$chart->id}";
			drupal_set_message(t("Added chart axis"));
		} catch(Exception $e){
			watchdog_exception("extjs_chart", $e);
			drupal_set_message(t("An error occurred while adding chart axis"), "error");
		}
	}
}

function _extjs_chart_axis_submit_numeric(&$form, &$form_state, &$axis){
	$o = new stdClass();
	$o->fields = $form_state["values"]["config"]["fields"];
	$o->position = $form_state["values"]["config"]["position"];
	$o->minimum = $form_state["values"]["config"]["minimum"];
	$o->maximum = $form_state["values"]["config"]["maximum"];
	$o->ticks_major = $form_state["values"]["config"]["major-ticks"];
	$o->ticks_minor = $form_state["values"]["config"]["minor-ticks"];
	$axis->config = $o;
}
