<?php
class ExtJsChartDb {
	public static function add($chart){
		$qry = db_insert("extjs_charts")
			->fields(array(
				"name"=>$chart->name,
				"description"=>$chart->description,
				"view_name"=>$chart->view_name,
				"service_endpoint"=>$chart->service_endpoint,
				"data_format"=>$chart->data_format,
				"height"=>$chart->height,
				"width"=>$chart->width,
				"legend_position"=>$chart->legend_position,
				"fields"=>drupal_json_encode($chart->fields)
			));
		return $qry->execute();
	}
	
	public static function update($chart){
		$qry = db_update("extjs_charts")
			->fields(array(
				"name"=>$chart->name,
				"description"=>$chart->description,
				"view_name"=>$chart->view_name,
				"data_format"=>$chart->data_format,
				"service_endpoint"=>$chart->service_endpoint,
				"height"=>$chart->height,
				"width"=>$chart->width,
				"legend_position"=>$chart->legend_position,
				"fields"=>drupal_json_encode($chart->fields)
			))
			->condition("id", $chart->id, "=");
		$qry->execute();
		return true;		
	}
	
	public static function get($chart_id){
		$qry = db_select("extjs_charts", "a")
			->fields("a");
		
		if(is_numeric($chart_id)){
			$qry->condition("id", $chart_id, "=");
		} else {
			$qry->condition("name", $chart_id, "=");
		}
		
		$chart = $qry->execute()->fetch(PDO::FETCH_OBJ);
		$chart->fields = drupal_json_decode($chart->fields);
		
		$chart->series = self::series_get_all($chart->id);
		$chart->axes = self::axis_get_all($chart->id);
		
		return $chart;
	}
	
	public static function all(){
		$qry = db_select("extjs_charts", "a")
			->fields("a");
		$charts = $qry->execute()->fetchAll(PDO::FETCH_OBJ);
		foreach($charts as $chart){
			$chart->fields = drupal_json_decode($chart->id);
			$chart->series = self::series_get_all($chart->id);
			$chart->axes = self::axis_get_all($chart->id);
		}
		
		return $charts;
	}
	
	public static function name_exists($name){
		$qry = db_select("extjs_charts", "a")
			->condition("name", $name, "=")
			->countQuery();
		$ret = $qry->execute()->fetch(PDO::FETCH_NUM);
		return $ret[0] > 0;
	}
	
	public static function series_add($chart, $series){
		$qry = db_insert("extjs_chart_series")
			->fields(array(
				"chart_id"=>$chart->id,
				"title"=>$series->title,
				"type"=>$series->type,
				"config"=>drupal_json_encode($series->configuration)
			));
		return $qry->execute();
	}
	
	public static function series_update($chart, $series){
		$qry = db_update("extjs_chart_series")
			->fields(array(
				"title"=>$series->title,
				"type"=>$series->type,
				"config"=>drupal_json_encode($series->configuration)
			))
			->condition("id", $series->id, "=");
		$qry->execute();
		return true;
	}
	
	public static function series_get($series_id){
		$qry = db_select("extjs_chart_series", "a")
			->fields("a")
			->condition("id", $series_id, "=");
		$series = $qry->execute()->fetch(PDO::FETCH_OBJ);
		if(isset($series) && isset($series->config)){
			$series->configuration = drupal_json_decode($series->config);
			return $series;
		}
	}
	
	public static function series_get_all($chart_id){
		$qry = db_select("extjs_chart_series", "a")
			->fields("a")
			->condition("chart_id", $chart_id, "=");
		$series = $qry->execute()->fetchAll(PDO::FETCH_OBJ);
		foreach($series as $s){
			$s->configuration = drupal_json_decode($s->config);
		}
		return $series;
	}
	
	public static function axis_add($chart, $axis){
		$qry = db_insert("extjs_chart_axes")
			->fields(array(
				"chart_id"=>$chart->id,
				"title"=>$axis->title,
				"type"=>$axis->type,
				"config"=>drupal_json_encode($axis->config)
			));
		return $qry->execute();
	}
	
	public static function axis_update($chart, $axis){
		$qry = db_update("extjs_chart_axes")
			->fields(array(
				"chart_id"=>$chart->id,
				"title"=>$axis->title,
				"type"=>$axis->type,
				"config"=>drupal_json_encode($axis->config)
			))
			->condition("id", $axis->id, "=");
		$qry->execute();
		return true;
	}
	
	public static function axis_get($axis_id){
		$qry = db_select("extjs_chart_axes", "a")
			->fields("a")
			->condition("id", $axis_id, "=");
		$axis = $qry->execute()->fetch(PDO::FETCH_OBJ);
		if(isset($axis) && isset($axis->config)){
			$axis->configuration = drupal_json_decode($axis->config);
			return $axis;
		}
	}
	
	public static function axis_get_all($chart_id){
		$qry = db_select("extjs_chart_axes", "a")
			->fields("a")
			->condition("chart_id", $chart_id, "=");
		$axes = $qry->execute()->fetchAll(PDO::FETCH_OBJ);
		foreach($axes as $a){
			$a->configuration = drupal_json_decode($a->config);
		}
		return $axes;
	}
}